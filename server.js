var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000

var path = require('path')

var requestjson = require('request-json')

var bodyparser = require('body-parser')
app.use(bodyparser.json())

app.use(function(req,res,next){
  res.header("Access-Control-Allow-Origin","*")
  res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept")
  next()
})

var urlMlabRaiz = "https://api.mlab.com/api/1/databases/kgarrido/collections/"
var apiKey = "?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt"
var clienteMlabRaiz
var clienteMlabRaiz1

var crypto = require('crypto')
var algorithm = 'aes-256-cbc'
var key = crypto.randomBytes(32)
var iv = crypto.randomBytes(16)

function encrypt(text) {
 let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(key), iv)
 let encrypted = cipher.update(text)
 encrypted = Buffer.concat([encrypted, cipher.final()])
 return { iv: iv.toString('hex'), encryptedData: encrypted.toString('hex') }
}

function decrypt(text) {
 let iv = Buffer.from(text.iv, 'hex')
 let encryptedText = Buffer.from(text.encryptedData, 'hex')
 let decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(key), iv)
 let decrypted = decipher.update(encryptedText)
 decrypted = Buffer.concat([decrypted, decipher.final()])
 return decrypted.toString()
}

app.listen(port)
console.log('todo list RESTful API server started on: ' + port)

app.post('/Login', function(req, res){
  //res.set("Access-Control-Allow-Headers", "Content-Type");
  var email = req.body.email
  var password = req.body.password

  var query = encrypt('q={"email":"'+email+'","password":"'+password+'"}')
  console.log(query)
  //console.log(decrypt(query))

  clienteMlabRaiz = requestjson.createClient(urlMlabRaiz + "Usuarios" + apiKey + "&" + decrypt(query))
  //console.log(urlMlabRaiz + "Usuarios" + apiKey + "&" + query)

  clienteMlabRaiz.get('', function(err,rresM,body){
    if (!err){
      if (body.length == 1){
        res.status(200).send("Usuario logado")
      } else {
        res.status(404).send("Usuario no encontrado")
      }
    }
  })
})

app.post('/Signup', function(req, res){
  //res.set("Access-Control-Allow-Headers", "Content-Type");
  var email = req.body.email
  var password = encrypt(req.body.password)
  var nombre = req.body.nombre

  var query = 'q={"email":"'+email+'","password":"'+password+'","nombre":"'+nombre+'"}'
  console.log(query)

  var query1 = 'q={"email":"'+email+'"}'
  console.log(query1)

  clienteMlabRaiz1 = requestjson.createClient(urlMlabRaiz + "Usuarios" + apiKey + "&" + query1)

  clienteMlabRaiz = requestjson.createClient(urlMlabRaiz + "Usuarios" + apiKey + "&" + query)
  console.log(urlMlabRaiz + "Usuarios" + apiKey + "&" + query)

  clienteMlabRaiz1.get('', function(err,rresM,body){
    if (!err){
      if (body.length == 0){
        clienteMlabRaiz.post('', req.body, function(err,rresM,body){
          if (!err){
            res.status(200).send("Usuario registrado")
          } else {
            res.status(404).send("Usuario no registrado")
          }
        })
      } else {
        res.status(404).send("Usuario que intenta registrar, ya existente")
      }
    }
  })
})

app.get('/Asesores', function(req, res){
  clienteMlabRaiz = requestjson.createClient(urlMlabRaiz + "Asesores" + apiKey)
  console.log(urlMlabRaiz + "Asesores" + apiKey)

  clienteMlabRaiz.get('', function(err,resM,body){
    if (!err){
      if (body.length >= 1){
        res.status(200).send(body)
      } else {
        res.status(404).send("No existen asesores listados")
      }
    }
  })
})

app.post('/Asesores', function(req, res){
  var idAsesor = req.body.idAsesor
  var nombre = req.body.nombre
  var apellido = req.body.apellido
  var apellidoM = req.body.apellidoM
  var tel = req.body.tel
  var canal = req.body.canal
  var status= req.body.status

  var query1 = 'q={"idAsesor":"'+idAsesor+'"}'
  console.log(query1)

  clienteMlabRaiz1 = requestjson.createClient(urlMlabRaiz + "Asesores" + apiKey + "&" + query1)

  var query = 'q={"idAsesor":"'+idAsesor+'","nombre":"'+nombre+'","apellido":"'+apellido+'","apellidoM":"'+apellidoM+'","tel":"'+tel+'","canal":"'+canal+'","status":"'+status+'"}'
  console.log(query)

  clienteMlabRaiz = requestjson.createClient(urlMlabRaiz + "Asesores" + apiKey + "&" + query)
  console.log(urlMlabRaiz + "Asesores" + apiKey + "&" + query)

  clienteMlabRaiz1.get('', function(err,rresM,body){
    if (!err){
      if (body.length == 0){
        clienteMlabRaiz.post('', req.body, function(err,rresM,body){
          if (!err){
            res.status(200).send("Asesor registrado ok")
          } else {
            res.status(404).send("Asesor no registrado")
          }
        })
      } else {
        res.status(404).send("Asesor que intenta registrar, ya existe")
      }
    }
  })
})

app.post('/ModAsesores', function(req, res){
  var idAsesor = req.body.idAsesor
  var nombre = req.body.nombre
  var apellido = req.body.apellido
  var apellidoM = req.body.apellidoM
  var tel = req.body.tel
  var canal = req.body.canal
  var status= req.body.status

  var query1 = 'q={"idAsesor":"'+idAsesor+'"}'
  console.log(query1)

  clienteMlabRaiz1 = requestjson.createClient(urlMlabRaiz + "Asesores" + apiKey + "&" + query1)

  var query = 'q={"nombre":"'+nombre+'","apellido":"'+apellido+'","apellidoM":"'+apellidoM+'","tel":"'+tel+'","canal":"'+canal+'","status":"'+status+'"}'
  console.log(query)

  clienteMlabRaiz = requestjson.createClient(urlMlabRaiz + "Asesores" + apiKey + "&" + query1 + "&" + query)
  console.log(urlMlabRaiz + "Asesores" + apiKey + "&" + query1 + "&" + query)

  clienteMlabRaiz1.get('', function(err,rresM,body){
    if (!err){
      if (body.length == 1){
        clienteMlabRaiz.put('', req.body, function(err,rresM,body){
          if (!err){
            res.status(200).send("Asesor modificado ok")
          } else {
            res.status(404).send("Asesor no modificado")
          }
        })
      } else {
        res.status(404).send("Asesor que intenta modificar, no existe")
      }
    }
  })
})

app.post('/DelAsesores', function(req, res){
  var idAsesor = req.body.idAsesor
  var id = req.body.id

  var query1 = 'q={"idAsesor":"'+idAsesor+'"}'
  console.log(query1)

  clienteMlabRaiz1 = requestjson.createClient(urlMlabRaiz + "Asesores" + apiKey + "&" + query1)
  console.log(urlMlabRaiz + "Asesores" + apiKey + "&" + query1)

  clienteMlabRaiz = requestjson.createClient(urlMlabRaiz + "Asesores/" + id + apiKey)
  console.log(urlMlabRaiz + "Asesores/" + id + apiKey)

  clienteMlabRaiz1.get('', function(err,rresM,body){
    if (!err){
      if (body.length == 1){
        clienteMlabRaiz.delete('', function(err,rresM,body){
          if (!err){
            res.status(200).send("Asesor eliminado ok")
          } else {
            res.status(404).send("Asesor no registrado")
          }
        })
      } else {
        res.status(404).send("Asesor que intenta eliminar,no existente")
      }
    }
  })
})

app.post('/BuscarAsesores', function(req, res){
  var idAsesor = req.body.idAsesor

  var query = 'q={"idAsesor":"'+idAsesor+'"}'
  console.log(query)

  clienteMlabRaiz = requestjson.createClient(urlMlabRaiz + "Asesores" + apiKey + "&" + query)
  console.log(urlMlabRaiz + "Asesores" + apiKey + "&" + query)

  clienteMlabRaiz.get('', function(err,resM,body){
    if (!err){
      if (body.length == 1){
        res.status(200).send(body)
      } else {
        res.status(404).send("No existen asesore")
      }
    }
  })
})

app.get('/Asignaciones', function(req, res){
  clienteMlabRaiz = requestjson.createClient(urlMlabRaiz + "Asignacion" + apiKey)
  console.log(urlMlabRaiz + "Asignacion" + apiKey)

  clienteMlabRaiz.get('', function(err,resM,body){
    if (!err){
      if (body.length >= 1){
        res.status(200).send(body)
      } else {
        res.status(404).send("No existen asesores listados")
      }
    }
  })
})

app.post('/Asignaciones', function(req, res){
  var idAsesor = req.body.idAsesor
  var contrato = req.body.contrato
  var contactCenter = req.body.contactCenter
  var familia= req.body.familia
  var mora = req.body.mora
  var producto = req.body.producto

  var query1 = 'q={"contrato":"'+contrato+'"}'
  console.log(query1)

  clienteMlabRaiz1 = requestjson.createClient(urlMlabRaiz + "Asignacion" + apiKey + "&" + query1)

  var query = 'q={"idAsesor":"'+idAsesor+'","contrato":"'+contrato+'","contactCenter":"'+contactCenter+'","familia":"'+familia+'","mora":"'+mora+'","producto":"'+producto+'"}'
  console.log(query)

  clienteMlabRaiz = requestjson.createClient(urlMlabRaiz + "Asignacion" + apiKey + "&" + query)
  console.log(urlMlabRaiz + "Asignacion" + apiKey + "&" + query)

  clienteMlabRaiz1.get('', function(err,rresM,body){
    if (!err){
      if (body.length == 0){
        clienteMlabRaiz.post('', req.body, function(err,rresM,body){
          if (!err){
            res.status(200).send("Asignacion registrada ok")
          } else {
            res.status(404).send("Asignacion no registrada")
          }
        })
      } else {
        res.status(404).send("Asignacion que intenta registrar, ya existe")
      }
    }
  })
})

app.post('/ModAsignaciones', function(req, res){
  var idAsesor = req.body.idAsesor
  var contrato = req.body.contrato
  var contactCenter = req.body.contactCenter
  var familia= req.body.familia
  var mora = req.body.mora
  var producto = req.body.producto

  var query1 = 'q={"contrato":"'+contrato+'"}'
  console.log(query1)

  clienteMlabRaiz1 = requestjson.createClient(urlMlabRaiz + "Asignacion" + apiKey + "&" + query1)

  var query = 'q={"idAsesor":"'+idAsesor+'","contactCenter":"'+contactCenter+'","familia":"'+familia+'","mora":"'+mora+'","producto":"'+producto+'"}'
  console.log(query)

  clienteMlabRaiz = requestjson.createClient(urlMlabRaiz + "Asignacion" + apiKey + "&" + query1 + "&" + query)
  console.log(urlMlabRaiz + "Asignacion" + apiKey + "&" + query1 + "&" + query)

  clienteMlabRaiz1.get('', function(err,rresM,body){
    if (!err){
      if (body.length == 1){
        clienteMlabRaiz.put('', req.body, function(err,rresM,body){
          if (!err){
            res.status(200).send("Asignacion modificada ok")
          } else {
            res.status(404).send("Asignacion no modificada")
          }
        })
      } else {
        res.status(404).send("Asignacion que intenta modificar, no existe")
      }
    }
  })
})

app.post('/DelAsignaciones', function(req, res){
  var contrato = req.body.contrato
  var id = req.body.id

  var query1 = 'q={"contrato":"'+contrato+'"}'
  console.log(query1)

  clienteMlabRaiz1 = requestjson.createClient(urlMlabRaiz + "Asignacion" + apiKey + "&" + query1)
  console.log(urlMlabRaiz + "Asignacion" + apiKey + "&" + query1)

  clienteMlabRaiz = requestjson.createClient(urlMlabRaiz + "Asignacion/" + id + apiKey)
  console.log(urlMlabRaiz + "Asignacion/" + id + apiKey)

  clienteMlabRaiz1.get('', function(err,rresM,body){
    if (!err){
      if (body.length == 1){
        clienteMlabRaiz.delete('', function(err,rresM,body){
          if (!err){
            res.status(200).send("Asignacion eliminada ok")
          } else {
            res.status(404).send("Asignacion no registrada")
          }
        })
      } else {
        res.status(404).send("Asignacion que intenta eliminar, no existente")
      }
    }
  })
})

app.post('/BuscarAsignaciones', function(req, res){
  var contrato = req.body.contrato

  var query = 'q={"contrato":"'+contrato+'"}'
  console.log(query)

  clienteMlabRaiz = requestjson.createClient(urlMlabRaiz + "Asignacion" + apiKey + "&" + query)
  console.log(urlMlabRaiz + "Asignacion" + apiKey + "&" + query)

  clienteMlabRaiz.get('', function(err,resM,body){
    if (!err){
      if (body.length == 1){
        res.status(200).send(body)
      } else {
        res.status(404).send("No existen asesore")
      }
    }
  })
})
